﻿using Dentistry.People;
using Dentistry.Services;
using System;

namespace Dentistry.Dentistry
{
    public abstract class PeopleDentistry<T>: IDentistry<T> where T : IPerson
    {
        public void MakeProcedure(T person)
        {
            Console.WriteLine($"Making procedure for: {person.Name}");
            this.SayAboutDiscount();
        }

        public void MakeProcedure<X>(T person, X service1) where X : IService
        {
            Console.WriteLine($"Making procedure {service1.ServiceName} for: {person.Name} ");
            this.SayAboutDiscount();
        }

        public void MakeProcedure<X, Z>(T person, X service1, Z service2) where X : IService where Z : IService
        {
            Console.WriteLine($"Making procedures {service1.ServiceName}" +
                              $" and {service2.ServiceName} for: {person.Name} ");
            this.SayAboutDiscount();
        }

        private void SayAboutDiscount()
        {
            Console.WriteLine("Come to us next time and you'll get discount");
        }
    }
}
