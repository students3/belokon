﻿using Dentistry.People;
using Dentistry.Services;

namespace Dentistry.Dentistry
{
    public interface IDentistry<T> //where T : IPerson
    {
        void MakeProcedure(T person);
        void MakeProcedure<X>(T person, X service1) where X : IService;
        void MakeProcedure<X, Z>(T person, X service1, Z service2) where X : IService where Z : IService ;
    }
}
