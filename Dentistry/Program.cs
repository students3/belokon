﻿using Dentistry.Dentistry;
using Dentistry.People;
using Dentistry.Services;
using System;

namespace Dentistry
{
    class Program
    {
        static void Main(string[] args)
        {
            var adult = new Adult { Name = "John" };
            var adultDentistry = new AdultDentistry();
            adultDentistry.MakeProcedure(adult, new Prosthetic(), new Whitening());

            Console.WriteLine();

            var child = new Child { Name = "Mike" };
            var childDentistry = new PediatricDentistry();
            childDentistry.MakeProcedure(child, new Bracket());

            Console.ReadLine();
        }
    }
}
