﻿
namespace Dentistry.People
{
    public interface IPerson
    {
        string Name { get; set; }
        PersonAgeCategory AgeCategory { get; }
    }
}
