﻿
namespace Dentistry.People
{
    public class Child : IPerson
    {
        public string Name { get; set; }
        public PersonAgeCategory AgeCategory { get; private set; }

        public Child()
        {
            this.AgeCategory = PersonAgeCategory.Children;
        }
    }
}
