﻿
namespace Dentistry.People
{
    public class Adult : IPerson
    {
        public string Name { get; set; }
        public PersonAgeCategory AgeCategory { get; private set; }

        public Adult()
        {
            this.AgeCategory = PersonAgeCategory.Adults;
        }
    }
}
