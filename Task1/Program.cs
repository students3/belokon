﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            //TODO: declare variable
            int NumberTask;
            NumberTask = int.Parse(Console.ReadLine());

            switch (NumberTask)
            {
                case 1:
                    Console.Clear();
                    Task1();
                    break;
                case 2:
                    Console.Clear();
                    Task2();
                    break;
                case 3:
                    Console.Clear();
                    Task3();
                    break;
                case 4:
                    Console.Clear();
                    Task4();
                    break;
                default:
                    Console.WriteLine("Incorrect number task");
                    break;
            }
            Console.ReadLine();
        }

        public static void Task1()
        {
            for (int i = 1; i <= 150; i++)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i);
                else
                {
                    continue;
                }
            }

        }
        public static void Task2()
        {
            for (int i = 50; i <= 200; i++)
            {
                if (i % 2 != 0)
                    Console.WriteLine(i);
                else
                {
                    continue;
                }
            }

        }
        public static void Task3()
        {
            int i = 0;
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            do
            {
                if (arrTemp[i] == 65)
                {
                    Console.WriteLine(i);
                    break;
                }

                i++;
            } while (i < arrTemp.Length);
        }
        public static void Task4()
        {
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            foreach (var item in arrTemp)
            {
                int num = item * 2;
                Console.WriteLine(num);
            }

        }
    }
}

