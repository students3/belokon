﻿using Business;
using Business.Interfaces;
using System;

namespace Lesson1
{
    sealed class Program
    {
        static void Main(string[] args)
        {
            IProduct[] products = GenerateProducts();
            SellProducts(products);
            PrintProducts(products);

            Console.ReadLine();
        }

        //////////////////

        private static IProduct[] GenerateProducts()
        {
            IProduct cake1 = new Cake();
            IProduct cake2 = new Cake();
            IProduct laptop1 = new Laptop();
            cake1.ProductName = "Roshen chokolet";
            cake1.Manufacturer = "Roshen corp";
            cake1.Price = 10.2;
            cake1.DateCreated = new DateTime(2017, 8, 5);

            cake2.ProductName = "cookies";
            cake2.Manufacturer = "Konti";
            cake2.Price = 40;
            cake2.DateCreated = new DateTime(2017, 6, 10);
            cake2.DateSold = new DateTime(2017, 6, 10);

            laptop1.ProductName = "Asus K53e";
            laptop1.Manufacturer = "Asus";
            laptop1.Price = 12000;
            laptop1.DateCreated = new DateTime(2017, 7, 1);

            return new IProduct[] { cake1, laptop1, cake2 };
        }

        private static void SellProducts(IProduct[] products)
        {
            for (int i = 0; i < products.Length; i++)
            {
                var item = products[i];

                if (item is Cake)
                {
                    IManager manager = new CakeManager();
                    manager.SellProduct(item, 10);
                }
                else if (item is Laptop)
                {
                    IManager manager = new LaptopManager();
                    manager.SellProduct(item, 3);
                }
            }
        }

        private static void PrintProducts(IProduct[] products)
        {
            for (int i = 0; i < products.Length; i++)
            {
                var product = products[i];

                string message = "Product " + product.ProductName + "\n" +
                                 "sold on " + product.DateSold?.ToString("yyyy MM") + "\n" +
                                 "in qty - " + product.Qty;
                Console.WriteLine(message);
            }
        }

        /*IProduct cake1 = new Cake();
            cake1.ProductName = "Roshen chokolet";
            cake1.Manufacturer = "Roshen corp";
            cake1.Price = 10.2;
            cake1.DateCreated = new DateTime(2017, 8, 5);

            IProduct cake2 = new Cake();
            cake2.ProductName = "cookies";
            cake2.Manufacturer = "Konti";
            cake2.Price = 40;
            cake2.DateCreated = new DateTime(2017, 6, 10);

            IProduct laptop1 = new Laptop();
            laptop1.ProductName = "Asus K53e";
            laptop1.Manufacturer = "Asus";
            laptop1.Price = 12000;
            laptop1.DateCreated = new DateTime(2017, 7, 1);

            IManager manager = new CakeManager();
            manager.SellProduct(cake1, 10);
            manager.SellProduct(cake2, 5);
            IManager manager2 = new LaptopManager();
            manager2.SellProduct(laptop1, 2);
            string message = "Product " + cake1.ProductName + "\n" +
                              "sold on " + cake1.DateSold?.ToString("yyyy MM") + "\n" +
                              "in qty - " + cake1.Qty;
            Console.WriteLine(message);
            string message2 = "Product " + laptop1.ProductName + "\n" +
                             "sold on " + laptop1.DateSold?.ToString("yyyy MM") + "\n" +
                             "in qty - " + laptop1.Qty;
            Console.WriteLine(message2);

            Console.ReadLine();*/

    }
}
