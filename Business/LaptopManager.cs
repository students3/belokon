﻿using Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
   public class LaptopManager : IManager
    {
        public void SellProduct(IProduct product, int qty)
        {
            product.DateCreated = DateTime.Now;
            product.Qty = qty;
        }
    }


}
