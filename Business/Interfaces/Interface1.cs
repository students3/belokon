﻿using System;


namespace Business.Interfaces
{
    public interface IProduct
    {
     string ProductName { get; set; }
     string Manufacturer { get; set; }
     DateTime DateCreated { get; set; }
     double Price { get; set; }
     DateTime? DateSold { get; set; }
     int Qty { get; set; }
    }
}
