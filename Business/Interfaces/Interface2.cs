﻿using System;


namespace Business.Interfaces
{
    public interface IManager
    {
        void SellProduct(IProduct product, int qty);
    }
}
