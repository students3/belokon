﻿using Business.Interfaces;
using System;


namespace Business
{
   public class CakeManager : IManager
    {
        public void SellProduct(IProduct product, int qty)
        {
            product.DateCreated = DateTime.Now;
            product.Qty = qty;
        }
    }
}
