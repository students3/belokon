﻿using Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class Laptop : IProduct
    {
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateCreated { get; set; }
        public double Price { get; set; }
        public DateTime? DateSold { get; set; }
        public int Qty { get; set; }
    }
}
