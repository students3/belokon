﻿using System;
using Business.Interfaces;

namespace Business
{
    public class Cake:IProduct
    {
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public DateTime DateCreated { get; set; }
        public double Price { get; set; }
        public DateTime? DateSold { get; set; }
        public int Qty { get; set; }
    }
}
